// load the things we need
var express = require('express');
var path = require('path');
var utils = require('./lib/utils.js');
var http = require('http');

var nodeMailer = require('nodemailer'),
    bodyParser = require('body-parser');

var app = express();
// set the view engine to ejs
app.set('view engine', 'ejs');
var config = require('./config.json');

app.use(config.url, express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
var port = utils.normalizePort(process.env.PORT || config.port);
var server;

const pg = require('pg');
const opt_db = {
  user: 'postgres',
  host: 'localhost',
  database: 'autosmart',
  password: "123456",
  port: '5432'};


 const mysql = require('mysql'); 

app.set('port', port);

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


// index page 
app.get('/', function(req, res) {

	res.render('pages/index');

});




server = http.createServer(app);
server.listen(port);